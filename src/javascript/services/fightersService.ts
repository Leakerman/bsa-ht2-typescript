import { IFighter } from './../interfaces';
import { callApi } from "../helpers/apiHelper";

class FighterService {
  async getFighters(): Promise<IFighter | IFighter[]> {
    try {
      const endpoint: string = "fighters.json";
      const apiResult = await callApi(endpoint, "GET");

      return apiResult;
    } catch (error) {
      throw error;
    }
  }

  async getFighterDetails(id: string): Promise<IFighter | IFighter[]> {
    try {
      const endpoint = `details/fighter/${id}.json`;
      const apiResult = await callApi(endpoint, "GET");

      return apiResult;
    } catch (error) {
      throw error;
    }
  }
}

export const fighterService = new FighterService();
