import { IFighter } from "./../interfaces";
import { fightersDetails, fighters } from "./mockData";

const API_URL: string =
  "https://api.github.com/repos/binary-studio-academy/stage-2-es6-for-everyone/contents/resources/api/";
const useMockAPI: boolean = true;

async function callApi(
  endpoint: string,
  method: string
): Promise<IFighter | IFighter[]> {
  const url = API_URL + endpoint;
  const options = {
    method,
  };

  return useMockAPI ? fakeCallApi(endpoint) : <Promise<IFighter>>fetch(
        url,
        options
      )
        .then((response) =>
          response.ok
            ? response.json()
            : Promise.reject(Error("Failed to load"))
        )
        .then((result) => JSON.parse(atob(result.content)))
        .catch((error) => {
          throw error;
        });
}

async function fakeCallApi(endpoint: string): Promise<IFighter | IFighter[]> {
  const response =
    endpoint === "fighters.json"
      ? <IFighter[]>fighters
      : getFighterById(endpoint);

  return new Promise((resolve, reject) => {
    setTimeout(
      () => (response ? resolve(response) : reject(Error("Failed to load"))),
      500
    );
  });
}

function getFighterById(endpoint: string): IFighter {
  const start = endpoint.lastIndexOf("/");
  const end = endpoint.lastIndexOf(".json");
  const id = endpoint.substring(start + 1, end);

  return <IFighter>fightersDetails.find((it) => it._id === id);
}

export { callApi };
