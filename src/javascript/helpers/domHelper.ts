interface IAttribute {
  [key: string]: string;
}

interface ICreateElementParams {
  tagName: string;
  className?: string;
  attributes?: IAttribute;
  innerText?: string;
}

export function createElement({
  tagName,
  className = "",
  attributes = {},
  innerText = "",
}: ICreateElementParams): HTMLElement {
  const element = document.createElement(tagName);

  if (className) {
    const classNames = className.split(" ").filter(Boolean);
    element.classList.add(...classNames);
  }

  Object.keys(attributes).forEach((key) =>
    element.setAttribute(key, attributes[key])
  );
  element.innerText = innerText;
  return element;
}
