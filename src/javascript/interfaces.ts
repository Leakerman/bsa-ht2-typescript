export interface IFighter {
  _id: string;
  name: string;
  health?: number;
  attack?: number;
  defense?: number;
  source: string;
  state?: { blockKey: string; canCrit: boolean; position: "left" | "right" };
}
