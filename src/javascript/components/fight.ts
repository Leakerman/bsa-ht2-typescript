import { IFighter } from "./../interfaces";
import { controls } from "../../constants/controls";

export async function fight(
  firstFighter: IFighter,
  secondFighter: IFighter
): Promise<IFighter> {
  return new Promise((resolve) => {
    const pressedKeys: Set<string> = new Set();

    firstFighter.state = {
      blockKey: controls.PlayerOneBlock,
      canCrit: true,
      position: "left",
    };

    secondFighter.state = {
      blockKey: controls.PlayerTwoBlock,
      canCrit: true,
      position: "right",
    };

    runFunctionOnKeys(
      pressedKeys,
      () =>
        fighterAttack(firstFighter, secondFighter, resolve, false, pressedKeys),
      [controls.PlayerOneAttack]
    );

    runFunctionOnKeys(
      pressedKeys,
      () =>
        fighterAttack(secondFighter, firstFighter, resolve, false, pressedKeys),
      [controls.PlayerTwoAttack]
    );

    runFunctionOnKeys(
      pressedKeys,
      () =>
        fighterAttack(firstFighter, secondFighter, resolve, true, pressedKeys),
      controls.PlayerOneCriticalHitCombination
    );

    runFunctionOnKeys(
      pressedKeys,
      () =>
        fighterAttack(secondFighter, firstFighter, resolve, true, pressedKeys),
      controls.PlayerTwoCriticalHitCombination
    );
  });
}

const keyUpHandler = (event: KeyboardEvent, pressedKeys: Set<string>) => {
  if (event.repeat) return;

  pressedKeys.delete(event.code);
};

const keyDownHandler = (
  event: KeyboardEvent,
  func: Function,
  codes: string[],
  pressedKeys: Set<string>
) => {
  if (event.repeat) return;

  pressedKeys.add(event.code);
  // is all keys pressed?
  for (let code of codes) {
    if (!pressedKeys.has(code)) return;
  }
  func();
};

function runFunctionOnKeys(
  pressedKeys: Set<string>,
  func: Function,
  codes: string[]
): void {
  document.addEventListener("keydown", (e) =>
    keyDownHandler(e, func, codes, pressedKeys)
  );
  document.addEventListener("keyup", (e) => keyUpHandler(e, pressedKeys));
}

function fighterAttack(
  attacker: IFighter,
  defender: IFighter,
  resolve: Function,
  isCrit: boolean,
  pressedKeys: Set<string>
): void {
  // atacker can not attack from block
  if (pressedKeys.has(defender.state!.blockKey) && !isCrit) {
    return;
  }

  const canCrit = attacker.state!.canCrit;
  const isAttackerBlocks = pressedKeys.has(attacker.state!.blockKey);

  let damage: number = 0;
  if (isCrit && canCrit && !isAttackerBlocks) {
    setCritDelay(attacker);

    damage = getCriticalHitPower(attacker);
  }
  if (!isCrit && !isAttackerBlocks) {
    damage = getDamage(attacker, defender);
  }

  const isDefeated = decreaseHealthBar(
    defender,
    damage,
    defender.state!.position
  );

  if (isDefeated) {
    resolve(attacker);
  }
}

function setCritDelay(fighter: IFighter): void {
  fighter.state!.canCrit = false;

  setTimeout(() => {
    fighter.state!.canCrit = true;
  }, 10000);
}

function decreaseHealthBar(
  fighter: IFighter,
  damage: number,
  position: "left" | "right"
): boolean {
  const healthBar = <HTMLElement>(
    document.getElementById(`${position}-fighter-indicator`)
  );
  const healthBarWidth = healthBar.style.width || "100";
  const decreasedHealthBarWidth =
    parseFloat(healthBarWidth) - (damage / <number>fighter.health) * 100;

  healthBar.style.width =
    decreasedHealthBarWidth > 0 ? `${decreasedHealthBarWidth}%` : "0";
  // check is fighter defeated
  return decreasedHealthBarWidth < 0;
}

export function getDamage(attacker: IFighter, defender: IFighter): number {
  const damage = getHitPower(attacker) - getBlockPower(defender);
  return damage < 0 ? 0 : damage;
}

export function getHitPower(fighter: IFighter): number {
  const randomNumber = Math.random() + 1;
  const { attack } = fighter;
  return randomNumber * <number>attack;
}

export function getCriticalHitPower(fighter: IFighter): number {
  return <number>fighter.attack * 2;
}

export function getBlockPower(fighter: IFighter): number {
  const randomNumber = Math.random() + 1;
  const { defense } = fighter;
  return randomNumber * <number>defense;
}
