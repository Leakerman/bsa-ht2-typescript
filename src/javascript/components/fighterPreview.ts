import { IFighter } from "./../interfaces";
import { createElement } from "../helpers/domHelper";

export function createFighterPreview(
  fighter: IFighter,
  position: "right" | "left"
): HTMLElement {
  const positionClassName: string =
    position === "right" ? "fighter-preview___right" : "fighter-preview___left";
  const fighterElement = createElement({
    tagName: "div",
    className: `fighter-preview___root card ${positionClassName}`,
  });

  if (!fighter) return fighterElement;

  const image = createFighterImage(fighter);
  const content = createFighterContent(fighter);

  fighterElement.append(image, content);

  return fighterElement;
}

function createFighterContent(fighter: IFighter): HTMLElement {
  const { health, attack, defense } = fighter;

  const content = createElement({
    tagName: "ul",
    className: "fighter-preview___content",
  });

  const healthItem = createFighterContentItem("Health", <number>health);
  const attackItem = createFighterContentItem("Attack", <number>attack);
  const defenseItem = createFighterContentItem("Defence", <number>defense);

  content.append(healthItem, attackItem, defenseItem);

  return content;
}

function createFighterContentItem(name: string, value: number): HTMLElement {
  const item = createElement({ tagName: "li" });
  item.innerHTML = `<b>${name}</b>: ${value}`;
  return item;
}

export function createFighterImage(fighter: IFighter): HTMLElement {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name,
  };
  const imgElement = createElement({
    tagName: "img",
    className: "fighter-preview___img",
    attributes,
  });

  return imgElement;
}
