import { IFighter } from "./../../interfaces";
import { showModal } from "./modal";
import { createFighterImage } from "../fighterPreview";

export function showWinnerModal(fighter: IFighter) {
  const title = `${fighter.name} won!`;
  const bodyElement = createFighterImage(fighter);
  const onClose = (): void => document.location.reload(true);
  showModal({ title, bodyElement, onClose });
}
